import copy
import os
from os.path import join
import numpy as np

import cv2
from PIL import Image
from keras_applications.imagenet_utils import preprocess_input
from keras import backend as K

def format_img_size(img, C):
    """ formats the image size based on config """
    img_min_side = float(C.im_size)
    (height, width, _) = img.shape

    if width <= height:
        ratio = img_min_side / width
        new_height = int(ratio * height)
        new_width = int(img_min_side)
    else:
        ratio = img_min_side / height
        new_width = int(ratio * width)
        new_height = int(img_min_side)
    img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
    return img, ratio


def format_img_channels(img, C):
    """ formats the image channels based on config """
    img = img[:, :, (2, 1, 0)]
    img = img.astype(np.float32)
    img[:, :, 0] -= C.img_channel_mean[0]
    img[:, :, 1] -= C.img_channel_mean[1]
    img[:, :, 2] -= C.img_channel_mean[2]
    img /= C.img_scaling_factor
    img = np.transpose(img, (2, 0, 1))
    img = np.expand_dims(img, axis=0)
    return img


def format_img2(img, C):
    """ formats an image for model prediction based on config """
    img, ratio = format_img_size(img, C)
    img = format_img_channels(img, C)
    return img, ratio



def format_img(img, C):
    img_min_side = float(C.im_size)
    (height, width, _) = img.shape

    if width <= height:
        f = img_min_side / width
        new_height = int(f * height)
        new_width = int(img_min_side)
    else:
        f = img_min_side / height
        new_width = int(f * width)
        new_height = int(img_min_side)
    fx = width / float(new_width)
    fy = height / float(new_height)
    img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
    img = img[:, :, (2, 1, 0)]
    img = img.astype(np.float32)
    img[:, :, 0] -= C.img_channel_mean[0]
    img[:, :, 1] -= C.img_channel_mean[1]
    img[:, :, 2] -= C.img_channel_mean[2]
    img /= C.img_scaling_factor
    img = np.transpose(img, (2, 0, 1))
    img = np.expand_dims(img, axis=0)
    return img, fx, fy

def format_img_prova(image_path):
    image = Image.open(image_path)
    # -------------------------------------#
    #   转换成RGB图片，可以用于灰度图预测。
    # -------------------------------------#
    image = image.convert("RGB")

    image_shape = np.array(np.shape(image)[0:2])
    old_width, old_height = image_shape[1], image_shape[0]
    old_image = copy.deepcopy(image)

    # ---------------------------------------------------------#
    #   给原图像进行resize，resize到短边为600的大小上
    # ---------------------------------------------------------#
    width, height = get_new_img_size(old_width, old_height)
    image = image.resize([width, height], Image.BICUBIC)
    photo = np.array(image, dtype=np.float64)

    # -----------------------------------------------------------#
    #   图片预处理，归一化。
    # -----------------------------------------------------------#
    photo = preprocess_input(np.expand_dims(photo, 0),data_format=K.image_data_format())
    return photo



def get_real_coordinates(ratio, x1, y1, x2, y2):
    real_x1 = int(round(x1 // ratio))
    real_y1 = int(round(y1 // ratio))
    real_x2 = int(round(x2 // ratio))
    real_y2 = int(round(y2 // ratio))
    return real_x1, real_y1, real_x2, real_y2


def get_new_img_size(width, height, img_min_side=600):
    if width <= height:
        f = float(img_min_side) / width
        resized_height = int(f * height)
        resized_width = int(img_min_side)
    else:
        f = float(img_min_side) / height
        resized_width = int(f * width)
        resized_height = int(img_min_side)

    return resized_width, resized_height


def write_files(img_data, all_dets, path,gt):
    filename = os.path.basename(img_data["filepath"].split(os.sep)[-1].replace(".jpg", ".txt"))

    folders = ["detection-results", "ground-truth"]

    folder = join(path,folders[0])
    if not os.path.exists(folder):
        os.makedirs(folder)

    file = open(join(folder, filename), "w")
    for i in all_dets:
        file.write(i['class'] + " " + i['prob'].__str__() + " " + i['x1'].__str__() + " " + i['y1'].__str__() + " " + i[
            'x2'].__str__() + " " + i['y2'].__str__() + "\n")

    folder =  join(path,folders[1])
    if not os.path.exists(folder):
        os.makedirs(folder)

   

    if gt == True:
        file = open(join(folder, filename), "w")

        for gt_box in img_data["bboxes"]:
            gt_x1 = float(gt_box['x1'])
            gt_x2 = float(gt_box['x2'])
            gt_y1 = float(gt_box['y1'])
            gt_y2 = float(gt_box['y2'])

            file.write(gt_box[
                           'class'] + " " + gt_x1.__str__() + " " + gt_y1.__str__() + " " + gt_x2.__str__() + " " + gt_y2.__str__() + "\n")

        file.close()
