# Faster-LTN: a neuro-symbolic, end-to-end object detection architecture


- The repository contains the code to implement Faster-LTN, the grounded ontology and the script to evaluate the models on PascalPart and Pascal Voc dataset.
- The ontology is retrieved from the paper *Learning and Reasoning in Logic Tensor Networks: Theory and Application for Semantic Image Interpretation* by Serafini Luciano, Donadello Ivan, d'Avila Garcez Artur 
- The initial FasterCNN model is cloned from https://github.com/you359/Keras-FasterRCNN
  
- All the material in the repository is the implementation of the paper accepted for publication *Faster-LTN: a neuro-symbolic, end-to-end object detection architecture* 
  by Manigrasso Francesco, Filomeno Davide Miro, Morra Lia , Lamberti Fabrizio.
- Download the repository with, for example, `git clone https://gitlab.com/grains2/Faster-LTN.git`.


## Data

- `pascalpart_dataset` it contains the  image dataset in pascal voc format with annotation in .xml file to reproduce performance of Faster CNN and Faster-LTN (available to `http://roozbehm.info/pascal-parts/pascal-parts.html`)
    - `Annotations`: the annotations in `.xml` format contains information related to bounding coordinates and classes of bounding box 
    - `ImageSets`: Contains the image splits in train and val dataset.
    - `JPEGImages`: the images in `.jpg` format.
    
  
  
- `pascalvoc_dataset`:   it contains the  image dataset in pascal voc format with annotation in .xml file to reproduce performance of Faster CNN and Faster-LTN (available to `http://host.robots.ox.ac.uk/pascal/VOC/voc2010/index.html`)
    - `Annotations`: the annotations in `.xml` format contains information related to bounding coordinates and classes of bounding box 
    - `ImageSets`: Contains the image splits in train and val dataset.
    - `JPEGImages`: the images in `.jpg` format.

- `imageset.npy`: contains the split used to train and test the model 
  

## Models

- `FasterCnn`: it contains the code to implement Faster CNN and Faster 
    - `imageset.npy`: a numpy array that contains imageset divisions in training and validation from trainval.txt set with a ratio of 80:20 respectively
    - `keras_frcnn`: the files related to the Faster CNN keras implementation and LTN block
    - `input`: the folder contains the output of the prediction of Faster CNN model
  
- `FasterLTN`: it contains the code to implement Faster CNN and Faster 
    - `imageset.npy`: a numpy array that contains imageset divisions in training and validation from trainval.txt set with a ratio of 80:20 respectively
    - `keras_frcnn`: the files related to the Faster CNN keras implementation and LTN block
    - `input`: the folder contains the output of the prediction of Faster CNN model

- `Weights`: the weights of the trained models can be accessed [here](https://www.dropbox.com/sh/5ww40hzcf31voyl/AABeia-QqLiGTi3fHp5Mk0Eha?dl=0) 
## How to train a FasterCnn 

```sh
$ python train_frcnn.py --train_path `(PASCAL VOC/PART FOLDER)`
```

## How to test a FasterCnn 
```sh
$ python test_frcnn.py --train_path `(PASCAL VOC/PART FOLDER)`
```

## How to train a FasterLTN

```sh
$ python train_frcnnltn.py --train_path `(PASCAL VOC/PART FOLDER)` --background (to include background predicate) --alpha ( to include alpha parameter) --half ( to use only half training dataset)
```



## How to test the grounded theories

```sh
$ python test_frcnnltn.py
```
- Results are in `reports/model_evaluation/types_partOf_models_evaluation.csv`
- More detailed results are in `reports/model_evaluation/training_without_constraints` and in `reports/model_evaluation/training_with_constraints`

## How to evaluate the model

```sh
$ python validation.py --train_path `(PASCAL VOC/PART FOLDER)` --background (to include background predicate) 
```
- Results of the detection are in `input` folder
- Result of map are in `results` folder, the map script is 
- Repository cloned from https://github.com/Cartucho/mAP.git

# Citation

If you make use of the dataset in your research, please cite our paper:

Manigrasso Francesco,Filomeno Davide Miro ,Morra Lia, Lamberti Fabrizio, 
["Faster-LTN: a neuro-symbolic, end-to-end object detection architecture"](), 
Proc. The 30th International Conference on Artificial Neural Networks.(ICANN 2021). 





# Contributors & Maintainers
Francesco Manigrasso, Filomeno Davide Miro ,Lia Morra,  and Fabrizio Lamberti
