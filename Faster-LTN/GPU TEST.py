
import tensorflow.compat.v1 as tf
tf.compat.v1.disable_v2_behavior()

mat1 = [0,1,2]
mat2 = [2,1,0]

with tf.device('/gpu:0'):
	tn1 = tf.compat.v1.placeholder(dtype= tf.int32, shape=[3])
	tn2 = tf.compat.v1.placeholder(dtype= tf.int32, shape=[3])
	tn3 = tn1 + tn2

with tf.compat.v1.Session() as sess:
	result = sess.run(tn3,{tn1:mat1,tn2:mat2})
	print(result)