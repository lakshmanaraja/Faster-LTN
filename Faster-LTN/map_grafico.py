
import os
import sys
import time
from optparse import OptionParser
from os.path import join

import cv2
import numpy as np
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras_frcnn import config
from keras_frcnn import config
from keras_frcnn import roi_helpers
from utils import write_files, format_img, format_img2, get_real_coordinates
import random

sys.setrecursionlimit(40000)


parser = OptionParser()

parser.add_option("-p", "--path", dest="test_path", help="Path to training data.")
parser.add_option("--background", action='store_true', help="bg missed.")
parser.add_option("--alpha", action='store_true', help="alpha missed.")

parser.add_option("-n", "--num_rois", dest="num_rois",
                  help="Number of ROIs per iteration. Higher means more memory use.", default=32)
parser.add_option("--config_filename", dest="config_filename", help=
"Location to read the metadata related to the training (generated when training).",
                  default="config.pickle")
parser.add_option("-o", "--parser", dest="parser", help="Parser to use. One of simple or pascal_voc",
                  default="pascal_voc"),

(options, args) = parser.parse_args()

if not options.test_path:  # if filename is not given
    parser.error('Error: path to test data must be specified. Pass --path to command line')

if options.parser == 'pascal_voc':
    from keras_frcnn.pascal_voc_parser import get_data
elif options.parser == 'simple':
    from keras_frcnn.simple_parser import get_data
else:
    raise ValueError("Command line option parser must be one of 'pascal_voc' or 'simple'")

config_output_filename = options.config_filename
# "test",
test_kind = ["test","trainval",]
# "test",
C = config.Config()

source_dir = C.result_folder

list_folder_test = os.listdir(source_dir)

list_weights = [os.path.join(dp, f) for dp, dn, fn in
                    os.walk(os.path.expanduser(join(source_dir))) for f in fn
                    if f.endswith(".hdf5")]
weights = {}
for f in list_weights:
    index = int(os.path.basename(f).split("_")[0])
    weights[index] = f

sd = sorted(weights.items(), reverse=True)

list_weights = [f[1] for f in sd]

C = config.Config()
# turn off any data augmentation at test time
C.use_horizontal_flips = False
C.use_vertical_flips = False
C.rot_90 = False

if C.network == 'resnet50':
    import keras_frcnn.resnet as nn
elif C.network == 'vgg':
    import keras_frcnn.vgg as nn
elif C.network == 'resnet101':
    import keras_frcnn.resnet101 as nn

img_path = options.test_path

class_mapping = C.class_mapping

cls = sorted(list(class_mapping.keys()))
class_mapping = {cls[i]: i for i in range(len(cls))}

if 'bg' not in class_mapping:
    class_mapping['bg'] = len(class_mapping)

class_mapping = {v: k for k, v in class_mapping.items()}
print(class_mapping)
class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}

if K.image_data_format() == 'channels_first':
    input_shape_img = (3, None, None)
    input_shape_features = (1024, None, None)
else:
    input_shape_img = (None, None, 3)
    input_shape_features = (None, None, 1024)

img_input = Input(shape=input_shape_img)
roi_input = Input(shape=(C.num_rois, 4))
feature_map_input = Input(shape=input_shape_features)

# define the base network (resnet here, can be VGG, Inception, etc)
shared_layers = nn.nn_base(img_input, trainable=True)

# define the RPN, built on the base layers
num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
rpn_layers = nn.rpn(shared_layers, num_anchors)

classifier = nn.classifierEvaluate(feature_map_input, roi_input, C.num_rois, len(class_mapping), 'linear',
                                   trainable=True, options=options)

model_rpn = Model(img_input, rpn_layers)
model_classifier_only = Model([feature_map_input, roi_input], classifier)

model_classifier = Model([feature_map_input, roi_input], classifier)

model_rpn.compile(optimizer='sgd', loss='mse')
model_classifier.compile(optimizer='sgd', loss='mse')

all_imgs, _, _ = get_data(options.test_path)

#,len(list_weights)-1
list_index_weights=[0]



for test in test_kind:
    source_path = join(source_dir, "MapResult", test)
    os.makedirs(source_path, exist_ok=True)

    if not os.path.exists(test + ".npy"):
        test_imgs = random.sample(test_imgs, 1000)
        with open(test + ".npy", 'wb') as f:

            np.save(f, np.array(test_imgs))
    else:
        with open(test + ".npy", 'rb') as f:
            test_imgs = np.load(f, allow_pickle=True)

    list_weights=["Experiment(FAS-306)\\195_model_all_PASCAL_VOC__4.597400951917309.hdf5"]

    for weight in list_weights:

        """

        if list_weights.index(weight) % int((len(list_weights) / 3)) != 0:
            continue
        """

        #test_imgs = [s for s in all_imgs if s['imageset'] == test]
        #test_imgs = random.sample(test_imgs, 1000)
        #print(len(test_imgs))
        T = {}
        P = {}
        pairs_partOF_true = []
        pairs_partOF_false = []
        data = []
        label = []

        all_dets_array = []
        all_imgs_array = []
        all_paths = []


        model_rpn.load_weights(weight, by_name=True)
        model_classifier.load_weights(weight, by_name=True)
        path = join(source_path, os.path.basename(weight))

        if os.path.exists(path):
            continue
        for idx, img_data in enumerate(test_imgs):
            if idx % 1000 == 0:
                print('{}/{}'.format(idx, len(test_imgs)))
            st = time.time()
            filepath = img_data['filepath']

            img = cv2.imread(filepath)

            X, fx, fy = format_img(img, C)

            if K.image_data_format() == 'channels_last':
                X = np.transpose(X, (0, 2, 3, 1))

            # get the feature maps and output from the RPN
            [Y1, Y2, F] = model_rpn.predict(X)

            R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_data_format(), overlap_thresh=0.7)

            # convert from (x1,y1,x2,y2) to (x,y,w,h)
            R[:, 2] -= R[:, 0]
            R[:, 3] -= R[:, 1]

            # apply the spatial pyramid pooling to the proposed regions
            bboxes = {}
            probs = {}
            features = {}

            for jk in range(R.shape[0] // C.num_rois + 1):
                ROIs = np.expand_dims(R[C.num_rois * jk:C.num_rois * (jk + 1), :], axis=0)
                if ROIs.shape[1] == 0:
                    break
                if jk == R.shape[0] // C.num_rois:
                    # pad R
                    curr_shape = ROIs.shape
                    target_shape = (curr_shape[0], C.num_rois, curr_shape[2])
                    ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
                    ROIs_padded[:, :curr_shape[1], :] = ROIs
                    ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
                    ROIs = ROIs_padded
                [P_regr,P_cls ] = model_classifier_only.predict([F, ROIs])

                for ii in range(P_cls.shape[1]):

                    if options.background:
                        #if np.argmax(P_cls[0, ii, :]) == (P_cls.shape[2] - 1):
                            #continue
                        index_max = np.argmax(P_cls[0, ii, :-1])
                    else:
                        index_max = np.argmax(P_cls[0, ii, :])


                    if P_cls[0, ii, :][index_max] >= C.score_thresh:
                        cls_name = class_mapping[index_max]
                        if cls_name not in bboxes:
                            bboxes[cls_name] = []
                            probs[cls_name] = []
                            features[cls_name] = []
                        (x, y, w, h) = ROIs[0, ii, :]
                        # cls_num = np.argmax(P_cls[0, ii, :])
                        cls_num = index_max
                        try:
                            (tx, ty, tw, th) = P_regr[0, ii, 4 * cls_num:4 * (cls_num + 1)]
                            tx /= C.classifier_regr_std[0]
                            ty /= C.classifier_regr_std[1]
                            tw /= C.classifier_regr_std[2]
                            th /= C.classifier_regr_std[3]
                            x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
                        except:
                            pass

                        bboxes[cls_name].append(
                            [C.rpn_stride * x, C.rpn_stride * y, C.rpn_stride * (x + w), C.rpn_stride * (y + h)])
                        if options.background:
                            probs[cls_name].append(np.max(P_cls[0, ii, :-1]))
                        else:
                            probs[cls_name].append(np.max(P_cls[0, ii, :]))
                        # features[cls_name].append(P_cls[0, ii, :-1])

            all_dets = []
            X, ratio = format_img2(img, C)
            for key in bboxes:
                bbox = np.array(bboxes[key])
                try:
                    new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]),
                                                                                overlap_thresh=0.3)
                    # new_boxes, new_probs = roi_helpers.non_max_suppression_fast(new_boxes, new_probs, overlap_thresh=0.5)
                    # new_boxes, new_probs = roi_helpers.non_max_suppression_fast(new_boxes, np.array(probs[key]), overlap_thresh=0.5)
                    for jk in range(new_boxes.shape[0]):
                        (x1, y1, x2, y2) = new_boxes[jk, :]

                        (real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)

                        det = {'x1': real_x1, 'x2': real_x2, 'y1': real_y1, 'y2': real_y2, 'class': key,
                               'prob': new_probs[jk]}
                        all_dets.append(det)

                        """


                        cv2.rectangle(img, (real_x1, real_y1), (real_x2, real_y2),
                                      (int(class_to_color[key][0]), int(class_to_color[key][1]),
                                       int(class_to_color[key][2])), 2)

                        textLabel = '{}: {}'.format(key, int(100 * new_probs[jk]))
                        (retval, baseLine) = cv2.getTextSize(textLabel, cv2.FONT_HERSHEY_COMPLEX, 1, 1)
                        textOrg = (real_x1, real_y1 - 0)

                        cv2.rectangle(img, (textOrg[0] - 5, textOrg[1] + baseLine - 5),
                                      (textOrg[0] + retval[0] + 5, textOrg[1] - retval[1] - 5), (0, 0, 0), 2)
                        cv2.rectangle(img, (textOrg[0] - 5, textOrg[1] + baseLine - 5),
                                      (textOrg[0] + retval[0] + 5, textOrg[1] - retval[1] - 5), (255, 255, 255), -1)
                        cv2.putText(img, textLabel, textOrg, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
                        
                        """
                        

                        # print('Elapsed time = {}'.format(time.time() - st))
                except:
                    print("error")

            all_dets_array.append(all_dets)
            all_imgs_array.append(img_data)
            all_paths.append(path)
            write_files(img_data, all_dets, (fx, fy), path, gt=True)


            """
            cv2.imshow("prova", img)
            cv2.waitKey()
            """


        """  

        for i in range(len(all_dets_array)):
            if list_weights.index(weight) == 0:
                write_files(all_imgs_array[i], all_dets_array[i], all_paths[i], True)
            else:
                write_files(all_imgs_array[i], all_dets_array[i], all_paths[i], False)

        """

