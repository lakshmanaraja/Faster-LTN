import os
import time
from optparse import OptionParser
from os.path import join

import cv2
from keras import Input, Model
from get_map import call_map
from keras_frcnn import roi_helpers, config
from keras_frcnn.pascal_voc_parser import get_data
from utils import format_img2, format_img, get_real_coordinates, write_files, format_img_prova
import numpy as np
import keras.backend as K

from keras_frcnn import resnet as nn


def validate_model(test_imgs, C, class_mapping, options, save_path):
    if True:

        cls = sorted(list(class_mapping.keys()))
        class_mapping = {cls[i]: i for i in range(len(cls))}
        if 'bg' not in classes_count:
            classes_count['bg'] = 0
            class_mapping['bg'] = len(class_mapping)

        print(class_mapping)
        class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}
        class_mapping = list(class_mapping.keys())

        if C.network == 'resnet50':
            num_features = 1024
        elif C.network == 'vgg':
            num_features = 512
        elif C.network == 'resnet101':
            num_features = 1024

        if K.image_data_format() == 'channels_first':
            input_shape_img = (3, None, None)
            input_shape_features = (num_features, None, None)
        else:
            input_shape_img = (None, None, 3)
            input_shape_features = (None, None, num_features)

        img_input = Input(shape=input_shape_img)
        roi_input = Input(shape=(C.num_rois, 4))
        feature_map_input = Input(shape=input_shape_features)

        # define the base network (resnet here, can be VGG, Inception, etc)
        shared_layers = nn.nn_base(img_input, trainable=True)

        # define the RPN, built on the base layers
        num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
        rpn_layers = nn.rpn(shared_layers, num_anchors)

        # classifier = nn.classifierEvaluate(feature_map_input, roi_input, C.num_rois, len(class_mapping),'linear', C.classifier_regr_std[0], C.classifier_regr_std[1], C.classifier_regr_std[2], C.classifier_regr_std[3],trainable=True)
        classifier = nn.classifierEvaluate(feature_map_input, roi_input, C.num_rois, len(class_mapping), 'linear',
                                           trainable=True, options=options)

        model_rpn = Model(img_input, rpn_layers)
        model_classifier_only = Model([feature_map_input, roi_input], classifier)

        model_classifier = Model([feature_map_input, roi_input], classifier)

        print('Loading weights from {}'.format(save_path))
        model_rpn.load_weights(save_path, by_name=True)
        model_classifier.load_weights(save_path, by_name=True)

        model_rpn.compile(optimizer='sgd', loss='mse')
        model_classifier.compile(optimizer='sgd', loss='mse')

        all_imgs = []

        classes = {}

        bbox_threshold = 0.3

        visualise = True

        for idx, img_data in enumerate(test_imgs):

            st = time.time()
            filepath = img_data['filepath']

            img = cv2.imread(filepath)

            X, ratio = format_img2(img, C)

            if K.image_data_format() != 'channels_first':
                X = np.transpose(X, (0, 2, 3, 1))

            # get the feature maps and output from the RPN
            [Y1, Y2, F] = model_rpn.predict(X)

            R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_data_format(), overlap_thresh=0.7)

            # convert from (x1,y1,x2,y2) to (x,y,w,h)
            R[:, 2] -= R[:, 0]
            R[:, 3] -= R[:, 1]

            # apply the spatial pyramid pooling to the proposed regions
            bboxes = {}
            probs = {}

            for jk in range(R.shape[0] // C.num_rois + 1):
                ROIs = np.expand_dims(R[C.num_rois * jk:C.num_rois * (jk + 1), :], axis=0)
                if ROIs.shape[1] == 0:
                    break

                if jk == R.shape[0] // C.num_rois:
                    # pad R
                    curr_shape = ROIs.shape
                    target_shape = (curr_shape[0], C.num_rois, curr_shape[2])
                    ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
                    ROIs_padded[:, :curr_shape[1], :] = ROIs
                    ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
                    ROIs = ROIs_padded

                [P_regr, P_cls] = model_classifier_only.predict([F, ROIs])

                for ii in range(P_cls.shape[1]):

                    if options.background:
                        index_max = np.argmax(P_cls[0, ii, : -1])
                    else:
                        index_max = np.argmax(P_cls[0, ii, :])

                    if P_cls[0, ii, index_max] < bbox_threshold:
                        continue

                    cls_name = class_mapping[index_max]

                    if cls_name not in bboxes:
                        bboxes[cls_name] = []
                        probs[cls_name] = []

                    (x, y, w, h) = ROIs[0, ii, :]

                    cls_num = index_max
                    try:
                        (tx, ty, tw, th) = P_regr[0, ii, 4 * cls_num:4 * (cls_num + 1)]
                        tx /= C.classifier_regr_std[0]
                        ty /= C.classifier_regr_std[1]
                        tw /= C.classifier_regr_std[2]
                        th /= C.classifier_regr_std[3]
                        x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
                    except:
                        pass
                    bboxes[cls_name].append(
                        [C.rpn_stride * x, C.rpn_stride * y, C.rpn_stride * (x + w), C.rpn_stride * (y + h)])

                    if options.background:
                        probs[cls_name].append(np.max(P_cls[0, ii, :-1]))
                    else:
                        probs[cls_name].append(np.max(P_cls[0, ii, :]))

                all_dets = []
                img = cv2.imread(filepath)
                X, ratio = format_img2(img, C)
                for key in bboxes:
                    bbox = np.array(bboxes[key])
                    try:
                        new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]),
                                                                                    overlap_thresh=0.3)
                        for jk in range(new_boxes.shape[0]):
                            (x1, y1, x2, y2) = new_boxes[jk, :]

                            (real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)

                            det = {'x1': real_x1, 'x2': real_x2, 'y1': real_y1, 'y2': real_y2, 'class': key,
                                   'prob': new_probs[jk]}
                            all_dets.append(det)
                        # print('Elapsed time = {}'.format(time.time() - st))
                    except Exception as e:
                        print(e)
                        continue

                folder_root = 'detection-results'

                if options.background:
                    folder_root = folder_root + "_bg"
                else:
                    folder_root = folder_root + "_no_bg"
                if options.alpha:
                    folder_root = folder_root + "_alpha"
                else:
                    folder_root = folder_root + "_no_alpha"

                path = "./input/" + folder_root

                if not os.path.exists(path):
                    os.makedirs(path)

                write_files(img_data, all_dets, path, gt=True)
    map = call_map()
    return map


if __name__ == "__main__":
    parser = OptionParser()

    parser.add_option("-p", "--path", dest="train_path", help="Path to training data.")
    parser.add_option("--exp_name", dest="exp_name", help="exp_name missed.")
    parser.add_option("--background", action='store_true', default=False, help="bg missed.")
    parser.add_option("--alpha", "--alpha", action='store_true', default=False, help="alpha missed.")
    parser.add_option("--axioms", action='store_true', default=False, help="axioms missed.")

    parser.add_option("--meta", action='store_true', default=False, help="meta missed.")
    parser.add_option("-o", "--parser", dest="parser", help="Parser to use. One of simple or pascal_voc",
                      default="pascal_voc")
    parser.add_option("--config_filename", dest="config_filename", help=
    "Location to store all the metadata related to the training (to be used when testing).",
                      default="config.pickle")
    parser.add_option("--output_weight_path", dest="output_weight_path", help="Output path for weights.",
                      default='./model_frcnn.hdf5')
    parser.add_option("--input_weight_path", dest="input_weight_path",
                      help="Input path for weights. If not specified, will try to load default weights provided by keras.")
    parser.add_option("--name", dest="name", help="Name to give at model")
    parser.add_option("--regularizer", type="float", dest="regularizer", help="regularizer element")

    (options, args) = parser.parse_args()

    all_imgs, classes_count, class_mapping = get_data(options.train_path, options.meta)
    test_imgs = [s for s in all_imgs if s['imageset'] == 'val']
    C = config.Config()
    validate_model(test_imgs, C, class_mapping, options,
                   options.input_weight_path)
