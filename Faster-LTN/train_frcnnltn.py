from __future__ import division

import csv
import datetime
import gc
import os
import pprint
import random
import sys
import time
from optparse import OptionParser
from os.path import join

import numpy as np
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import generic_utils
from tensorflow.python.keras.models import load_model

import keras_frcnn.ltn as ltn
import keras_frcnn.roi_helpers as roi_helpers
from keras_frcnn import config, data_generators
from keras_frcnn import losses as losses_function
import tensorflow as tf
from validation import validate_model

import tensorflow as tf

cfg = tf.compat.v1.ConfigProto()
cfg.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=cfg)


def write_row(filename, row):
    with open(filename, mode='a') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        employee_writer.writerow(row)


def defineGT(labels, num_classes, batch_size):
    y = []
    for i in range(num_classes):
        y_i = np.zeros((batch_size, 1))
        for j in range(batch_size):
            label = np.argmax(labels[0, j, :])
            if label == i:
                y_i[j, 0] = 1

        y.append(np.expand_dims(y_i, axis=0))

    return y


sys.setrecursionlimit(40000)

parser = OptionParser()

parser.add_option("-p", "--path", dest="train_path", help="Path to training data.")
parser.add_option("--exp_name", dest="exp_name", help="exp_name missed.")
parser.add_option("--background", action='store_true', default=False, help="bg missed.")
parser.add_option("--axioms", action='store_true', default=False, help="axioms missed.")
parser.add_option("--half", action='store_true', default=False, help="meta missed.")
parser.add_option("--alpha", "--alpha", action='store_true', default=False, help="alpha missed.")
parser.add_option("-o", "--parser", dest="parser", help="Parser to use. One of simple or pascal_voc",
                  default="pascal_voc")
parser.add_option("--config_filename", dest="config_filename", help=
"Location to store all the metadata related to the training (to be used when testing).",
                  default="config.pickle")
parser.add_option("--output_weight_path", dest="output_weight_path", help="Output path for weights.",
                  default='./model_frcnn.hdf5')
parser.add_option("--input_weight_path", dest="input_weight_path",
                  help="Input path for weights. If not specified, will try to load default weights provided by keras.")
parser.add_option("--name", dest="name", help="Name to give at model")
parser.add_option("--regularizer", type="float", dest="regularizer", help="regularizer element")
parser.add_option("--base_net", dest="base_net", help="base_net missed.")

(options, args) = parser.parse_args()

if not options.train_path:  # if filename is not given

    parser.error(
        'Error: path to training data must be specified. Pass --path  --exp_name  or bg or alpha not specified')

if options.parser == 'pascal_voc':
    from keras_frcnn.pascal_voc_parser import get_data
elif options.parser == 'simple':
    from keras_frcnn.simple_parser import get_data
else:
    raise ValueError("Command line option parser must be one of 'pascal_voc' or 'simple'")

C = config.Config()
os.makedirs(C.experiment_name, exist_ok=True)

C.partOF = options.axioms

if C.network == 'vgg':
    from keras_frcnn import vgg as nn
elif C.network == 'resnet50':
    from keras_frcnn import resnet as nn

elif C.network == 'resnet101':
    from keras_frcnn import resnet101 as nn
else:
    print('Not a valid model')
    raise ValueError

all_imgs, classes_count, class_mapping = get_data(options.train_path, options.half)
cls = sorted(list(class_mapping.keys()))
class_mapping = {cls[i]: i for i in range(len(cls))}

if options.alpha:
    C.alpha = True
    examples_per_classes = [x[1] for x in sorted([x for x in classes_count.items()], key=lambda x: x[0])]
    beta = 0.9999
    if options.background:
        num_classes = len(examples_per_classes) + 1
    else:
        num_classes = len(examples_per_classes)
    tot = np.sum(examples_per_classes)
    examples_per_classes_p = [e / tot for e in examples_per_classes]

    effective_pos = [int(C.num_rois / C.pos_split) * e for e in examples_per_classes_p]
    effective_pos.append(int(C.num_rois / C.pos_split))
    effective_pos = 1.0 - np.power(beta, effective_pos)
    weights_pos = (1.0 - beta) / np.array(effective_pos)
    weights_pos = weights_pos / np.sum(weights_pos) * int(num_classes)

    neg_examples = C.num_rois - int(C.num_rois / C.pos_split)
    effective_neg = [neg_examples + neg_examples * (1 - e) for e in examples_per_classes_p]
    effective_neg.append(neg_examples)
    effective_neg = 1.0 - np.power(beta, effective_neg)
    weights_neg = (1.0 - beta) / np.array(effective_neg)
    weights_neg = weights_neg / np.sum(weights_neg) * int(num_classes)

    print(weights_pos)
    print(weights_neg)
else:
    C.alpha = False
    weights_pos = None
    weights_neg = None

if 'bg' not in classes_count:
    classes_count['bg'] = 0
    class_mapping['bg'] = len(class_mapping)

C.class_mapping = class_mapping
print(C.class_mapping)

inv_map = {v: k for k, v in class_mapping.items()}

print('Training images per class:')
pprint.pprint(classes_count)
print('Num classes (including bg) = {}'.format(len(classes_count)))

random.shuffle(all_imgs)

num_imgs = len(all_imgs)

train_imgs = [s for s in all_imgs if s['imageset'] == 'train']
test_imgs = [s for s in all_imgs if s['imageset'] == 'val']

data_gen_train = data_generators.get_anchor_gt(train_imgs, classes_count, C, nn.get_img_output_length,
                                               K.image_data_format(), mode='train')
data_gen_val = data_generators.get_anchor_gt(test_imgs, classes_count, C, nn.get_img_output_length,
                                             K.image_data_format(), mode='val')

if K.image_data_format() == 'channels_first':
    input_shape_img = (3, None, None)
else:
    input_shape_img = (None, None, 3)

img_input = Input(shape=input_shape_img)
roi_input = Input(shape=(None, 4))

if options.background:
    predicate_number = len(classes_count)
    C.background = True
else:
    predicate_number = len(classes_count) - 1
    C.background = False

Y = [Input(shape=(C.num_rois, 1)) for i in range(predicate_number)]

Y_partOf = Input(shape=(int(C.num_rois / C.pos_split) * int(C.num_rois / C.pos_split), 1))

if C.l2_regularizer != None:
    regularizer = tf.keras.regularizers.l2(C.l2_regularizer)
else:
    regularizer = None

# define the base network (resnet here, can be VGG, Inception, etc)
shared_layers = nn.nn_base(img_input, trainable=True)
# define the RPN, built on the base layers
num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
rpn = nn.rpn(shared_layers, num_anchors)
classifier = nn.classifier(shared_layers, roi_input, C.num_rois, len(class_mapping), 'luk', 'focal_loss_logsum',
                           'linear', 1, Y, Y_partOf, classes=list(class_mapping.keys()),
                           std_x=C.classifier_regr_std[0], std_y=C.classifier_regr_std[1],
                           std_w=C.classifier_regr_std[2], std_h=C.classifier_regr_std[3],
                           weights=(weights_pos, weights_neg), options=options, kernel_regularizer=regularizer,
                           config=C)

model_rpn = Model(img_input, rpn[:2])
model_classifier = Model([img_input, roi_input] + Y + [Y_partOf], classifier)
model_all = Model([img_input, roi_input] + Y + [Y_partOf], rpn[:2] + classifier)


if options.base_net:
    C.base_weights = options.base_net

if "Experiment" in C.base_weights:
    start =int(os.path.basename(C.base_weights).split("_")[0])
else:
    start = 0

print('loading weights from {}'.format(C.base_weights))
model_rpn.load_weights(C.base_weights, by_name=True)
model_classifier.load_weights(C.base_weights, by_name=True)


def step_decay(epoch, lr):

    if epoch == 90:
        lr = C.rpn_lr / 10



    return lr


optimizer = Adam(lr=C.rpn_lr,decay=0.0005)
optimizer_classifier = Adam(lr=C.classifier_lr,decay=0.0005)
model_rpn.compile(optimizer=optimizer,
                  loss=[losses_function.rpn_loss_cls(num_anchors), losses_function.rpn_loss_regr(num_anchors)])
model_classifier.compile(optimizer=optimizer_classifier,
                         loss=[losses_function.class_loss_regr(len(classes_count) - 1),
                               ltn.ltn_loss('sum', C.default_smooth_factor)])

model_all.compile(optimizer='sgd', loss='mae')
losses = np.zeros((C.num_iterations, 5))
rpn_accuracy_rpn_monitor = []
rpn_accuracy_for_epoch = []

start_time = time.time()

best_loss = np.inf

class_mapping_inv = {v: k for k, v in class_mapping.items()}
print('Starting training')
vis = True
cycle = 0
iter_num = 0
id_experiment ="exp_1"
C.result_folder = id_experiment
print('Num train samples {}'.format(len(train_imgs)))
print('Num val samples {}'.format(len(test_imgs)))
decreased = False

for epoch_num in range(start, C.num_epochs):

    lr = K.get_value(model_rpn.optimizer.lr)
    print("OPTIMIZER LR " + lr.__str__())

    K.set_value(model_rpn.optimizer.lr, step_decay(epoch_num, K.get_value(model_rpn.optimizer.lr)))
    K.set_value(model_classifier.optimizer.lr, step_decay(epoch_num, K.get_value(model_classifier.optimizer.lr)))

    progbar = generic_utils.Progbar(C.num_iterations)
    print('Epoch {}/{}'.format(epoch_num + 1, C.num_epochs))

    while True:

        log_file = open(join(C.experiment_name, 'losses_{}.txt'.format(C.experiment_name)), 'a')

        if len(rpn_accuracy_rpn_monitor) == C.num_iterations and C.verbose:
            mean_overlapping_bboxes = float(sum(rpn_accuracy_rpn_monitor)) / len(rpn_accuracy_rpn_monitor)
            rpn_accuracy_rpn_monitor = []
            print('Average number of overlapping bounding boxes from RPN = {} for {} previous iterations'.format(
                mean_overlapping_bboxes, C.num_iterations))
            if mean_overlapping_bboxes == 0:
                print(
                    'RPN is not producing bounding boxes that overlap the ground truth boxes. Check RPN settings or keep training.')

        X, Y, img_data = next(data_gen_train)

        loss_rpn = model_rpn.train_on_batch(X, Y)

        P_rpn = model_rpn.predict_on_batch(X)

        R = roi_helpers.rpn_to_roi(P_rpn[0], P_rpn[1], C, K.image_data_format(), use_regr=True, overlap_thresh=0.7,
                                   max_boxes=300)
        # note: calc_iou converts from (x1,y1,x2,y2) to (x,y,w,h) format
        X2, Y1, Y2, Y3, IouS = roi_helpers.calc_iou_partOf(R, img_data, C, class_mapping)

        if X2 is None:
            rpn_accuracy_rpn_monitor.append(0)
            rpn_accuracy_for_epoch.append(0)
            continue

        neg_samples = np.where(Y1[0, :, -1] == 1)
        pos_samples = np.where(Y1[0, :, -1] == 0)

        if len(neg_samples) > 0:
            neg_samples = neg_samples[0]
        else:
            neg_samples = []

        if len(pos_samples) > 0:
            pos_samples = pos_samples[0]
        else:
            pos_samples = []

        rpn_accuracy_rpn_monitor.append(len(pos_samples))
        rpn_accuracy_for_epoch.append((len(pos_samples)))

        if C.num_rois > 1:
            if len(pos_samples) < C.num_rois // C.pos_split:
                selected_pos_samples = pos_samples.tolist()
            else:
                if len(pos_samples) > 0:
                    selected_pos_samples = np.random.choice(pos_samples, C.num_rois // C.pos_split,
                                                            replace=False).tolist()
                else:
                    selected_pos_samples = []
            try:
                if len(neg_samples) > 0:
                    selected_neg_samples = np.random.choice(neg_samples, C.num_rois - len(selected_pos_samples),
                                                            replace=False).tolist()
                else:
                    selected_neg_samples = []
            except:
                if len(neg_samples) > 0:
                    selected_neg_samples = np.random.choice(neg_samples, C.num_rois - len(selected_pos_samples),
                                                            replace=True).tolist()
                else:
                    selected_neg_samples = []

            sel_samples = selected_pos_samples + selected_neg_samples
        else:
            # in the extreme case where num_rois = 1, we pick a random pos or neg sample
            selected_pos_samples = pos_samples.tolist()
            selected_neg_samples = neg_samples.tolist()
            if np.random.randint(0, 2):
                sel_samples = random.choice(neg_samples)
            else:
                sel_samples = random.choice(pos_samples)
        Y3_selected = []
        for i in sel_samples[0:C.num_rois // C.pos_split]:
            for j in sel_samples[0:C.num_rois // C.pos_split]:
                Y3_selected.append(Y3[i][j])
        Y3_selected = np.array(Y3_selected)
        Y3_selected = np.expand_dims(np.expand_dims(Y3_selected, 0), 2)

        y = defineGT(Y1[:, sel_samples, :], predicate_number, C.num_rois)
        num_classes = len(classes_count)
        o = np.ones((1, 1, 1950))

        # model_classifier = Model([img_input, roi_input] + Y + [Y_partOf], classifier)

        loss_class = model_classifier.train_on_batch([X, X2[:, sel_samples, :]] + y + [Y3_selected],
                                                     [Y2[:, sel_samples, :], o])

        # model_classifier = Model([img_input, roi_input] + Y, classifier)

        losses[iter_num, 0] = loss_rpn[1]
        losses[iter_num, 1] = loss_rpn[2]
        losses[iter_num, 2] = loss_class[1]
        losses[iter_num, 3] = loss_class[2]

        for i in range(4):
            log_file.write(join(C.experiment_name, '{}\t'.format(np.mean(losses[:iter_num, i]))))
        log_file.write('\n')

        log_file.close()
        iter_num += 1

        progbar.update(iter_num,
                       [('rpn_cls', np.mean(losses[:iter_num, 0])), ('rpn_regr', np.mean(losses[:iter_num, 1])),
                        ('detector_regr', np.mean(losses[:iter_num, 2])), ('ltn', np.mean(losses[:iter_num, 3]))])

        if iter_num == C.num_iterations:
            loss_rpn_cls = np.mean(losses[:, 0])
            loss_rpn_regr = np.mean(losses[:, 1])
            loss_class_regr = np.mean(losses[:, 2])
            loss_class_cls = np.mean(losses[:, 3])
            class_acc = np.mean(losses[:, 4])
            write_row(join(C.experiment_name, "ouput.csv"), [epoch_num, "loss_rpn_cls", loss_rpn_cls])
            write_row(join(C.experiment_name, "ouput.csv"), [epoch_num, "loss_rpn_regr", loss_rpn_regr])
            write_row(join(C.experiment_name, "ouput.csv"), [epoch_num, "loss_class_cls", loss_class_cls])
            write_row(join(C.experiment_name, "ouput.csv"), [epoch_num, "loss_class_regr", loss_class_regr])
            write_row(join(C.experiment_name, "ouput.csv"), [epoch_num, "Classifier accuracy", class_acc])

            print('Elapsed time: {}'.format(time.time() - start_time))

            # file_writer_loss.flush()

            log_file_end = open(join(C.experiment_name, 'losses_end_{}.txt'.format(C.experiment_name)), 'a')
            for i in range(4):
                log_file_end.write('{}\t'.format(np.mean(losses[:, i])))
            log_file_end.write('\n')

            log_file_end.close()
            mean_overlapping_bboxes = float(sum(rpn_accuracy_for_epoch)) / len(rpn_accuracy_for_epoch)
            rpn_accuracy_for_epoch = []

            if C.verbose:
                print('Mean number of bounding boxes from RPN overlapping ground truth boxes: {}'.format(
                    mean_overlapping_bboxes))
                print('Classifier accuracy for bounding boxes from RPN: {}'.format(class_acc))
                print('Loss RPN classifier: {}'.format(loss_rpn_cls))
                print('Loss RPN regression: {}'.format(loss_rpn_regr))
                print('Loss Detector classifier: {}'.format(loss_class_cls))
                print('Loss Detector regression: {}'.format(loss_class_regr))
                print('Elapsed time: {}'.format(time.time() - start_time))

            curr_loss = loss_rpn_cls + loss_rpn_regr + loss_class_cls + loss_class_regr

            iter_num = 0
            start_time = time.time()

            if curr_loss < best_loss:
                if C.verbose:
                    print('Total loss decreased from {} to {}, saving weights'.format(best_loss, curr_loss))

                best_loss = curr_loss

            if epoch_num % 5 == 0:
                save_path = join(C.result_folder, '{}_model_all__{}.hdf5'.format(epoch_num, curr_loss))
                model_all.save_weights(save_path)
                C.base_weights = save_path
            break

print('Training complete, exiting.')
