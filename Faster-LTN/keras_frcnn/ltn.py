import sys
from keras.layers import Layer, Activation
import tensorflow as tf






class Predicate(Layer):
    def __init__(self, num_features, k, i,config, **kwargs):
        super(Predicate, self).__init__(**kwargs)
        self.output_dim = 1
        self.num_features = num_features
        self.k = k
        self.name_ = 'predicate_{}'.format(i)
        if config.l2_l2_regularizer_ltn != None:
            self.regularizer = tf.keras.regularizers.l2(config.l2_l2_regularizer_ltn)
        else:
            self.regularizer = None

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        mn = self.num_features
        label = self.name_
        layers = self.k

        self.up = self.add_weight(name='up' + self.name_,  # 1 x k
                                  shape=(self.k, 1),
                                  initializer='ones',
                                  trainable=True, regularizer=self.regularizer)

        self.Wp = self.add_weight(name='Wp' + self.name_,
                                  shape=(self.k, mn, mn),
                                  initializer='random_normal',
                                  trainable=True, regularizer=self.regularizer)
        self.Vp = self.add_weight(name='Vp' + self.name_,
                                  shape=(self.k, mn),
                                  initializer='random_normal',
                                  trainable=True, regularizer=self.regularizer)
        self.bp = self.add_weight(name='bp' + self.name_,
                                  shape=(1, self.k),
                                  initializer='ones',
                                  trainable=True, regularizer=self.regularizer)
        self.bp = self.bp * -1

        self.parameters = [self.Wp, self.Vp, self.bp, self.up]

        super(Predicate, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        # (1,batch,features) -> (batch,features)
        # x = tf.Print(x,[x],"x:")
        X = tf.squeeze(x)
        # X = tf.Print(X,[X],"X:")
        # X = x
        XW = tf.matmul(tf.tile(tf.expand_dims(X, 0), [self.k, 1, 1]), self.Wp)
        #        XW = tf.Print(XW,[XW],"XW:")
        XWX = tf.squeeze(tf.matmul(tf.expand_dims(X, 1), tf.transpose(a=XW, perm=[1, 2, 0])))
        #       XWX = tf.Print(XWX,[XWX],"XWX:")
        XV = tf.matmul(X, tf.transpose(a=self.Vp))
        #      XV = tf.Print(XV,[XV],"XV:")
        gX = tf.matmul(tf.tanh(XWX + XV + self.bp), self.up)
        #     gX = tf.Print(gX,[gX],"gX:")
        h = tf.sigmoid(gX)

        return h


def smooth(parameters, default_smooth_factor):
    norm_of_omega = tf.reduce_sum(input_tensor=tf.expand_dims(tf.concat(
        [tf.expand_dims(tf.reduce_sum(input_tensor=tf.square(par)), 0) for par in parameters], axis=0), 1))
    return tf.multiply(default_smooth_factor, norm_of_omega)


def smooth_labels(labels, factor=0.1):
    # smooth the labels
    labels = tf.multiply(labels, (tf.subtract(tf.cast(1, dtype=tf.float32), tf.cast(factor, dtype=tf.float32))))
    labels = tf.add(labels, (tf.divide(factor, labels.shape[1])))
    return labels


def ltn_loss(type, smooth_factor):
    def ltn_loss(y_true, y_pred):
        if type == 'hmean':
            return -tf.compat.v1.div(tf.reduce_sum(input_tensor=y_true),
                                     tf.reduce_sum(input_tensor=tf.compat.v1.div(y_true, y_pred),
                                                   keepdims=True) + tf.constant(1e-15))
        elif type == 'sum':
            # model.summary()
            sum_reduced= tf.reduce_sum(input_tensor=y_pred, keepdims=True)
            return  sum_reduced

        else:
            return None

    return ltn_loss
